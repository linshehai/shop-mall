local cjson = require("cjson" )
local producer = require("resty.kafka.producer")

local broker_list = {
    {
        host = "192.168.1.21",
        port = 9092,
    }
}

local headers = ngx.req.get_headers()
local ip = headers['X-REAL-IP'] or headers['X-FORWARD-FOR'] or ngx.var.remote_addr or '0.0.0.0'
local message = {}
message["access_time"] = os.date('%Y-%m-%d %H:%m:%s')
message["ip"] = ip
message["uri"] = ngx.var.uri
-- this is async producer_type and bp will be reused in the whole nginx worker
local bp = producer:new(broker_list, { producer_type = "async" })

local ok, err = bp:send("goodsdetail", nil, cjson.encode(message))
if not ok then
    ngx.say("send err:", err)
    return
end