package com.hy.mall.gateway.hot;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class HotQueue {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    public void queue(String account,Long skuId,int num){
        boolean hasGoods = redisTemplate.boundHashOps("hotGoods").hasKey(skuId);
        //判断是否存在该商品
        /*if(!hasGoods){
            return;
        }*/
        String key = "order_queue"+account+skuId;
        //判断当前商品是否已经在排队，如果已经排队则返回
        Long queued = redisTemplate.boundValueOps(key).increment(1);
        /*if(queued>1){
            return;
        }*/

        //十分钟后可以继续抢购
        redisTemplate.boundValueOps(key).expire(10, TimeUnit.MINUTES);

        Map<String,Object> userOrder = new HashMap<>();
        userOrder.put("account",account);
        userOrder.put("skuId",skuId);
        userOrder.put("num",num);
        Message<String> message = MessageBuilder.withPayload(JSON.toJSONString(userOrder)).build();
        rocketMQTemplate.convertAndSend("order-queue",message);
    }
}
