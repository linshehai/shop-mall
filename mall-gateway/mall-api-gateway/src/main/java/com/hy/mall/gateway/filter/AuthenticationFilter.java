package com.hy.mall.gateway.filter;

import com.hy.mall.utils.JwtTokenUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * 权限认证过滤器
 */
@Configuration
public class AuthenticationFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        RequestPath path = request.getPath();
        String realPath = path.value();

        //登陆接口放行
        if("/admin/login".equals(realPath)){
            return chain.filter(exchange);
        }

        //判断是否已经登陆

        String account = "kevin";

        HttpHeaders headers = request.getHeaders();
        //获取用户token
        String authorization = headers.getFirst("Authorization");
        try {
            //验证token合法性,成功解析则token合法
            Map<String, Object> claim = JwtTokenUtils.parseToken(authorization);
            //获取用户账号信息
            account = (String) claim.get("username");
            String userId = String.valueOf(claim.get("userId"));
            String finalAccount = account;
            request = request.mutate().headers(httpHeaders -> {
                httpHeaders.set("username", finalAccount);
                httpHeaders.set("userId", String.valueOf(userId));
            }).build();
            //重新构建request
            exchange = exchange.mutate().request(request).build();
        }catch (Exception e){
            //解析失败，token无效
            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.setComplete();
        }

        //如果没有权限
        /*if(true){
            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.setComplete();
        }*/
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 10003;
    }
}
