package com.hy.mall.file;


import io.minio.*;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class MinioServiceImpl implements FileService{

    private MinioClient minioClient;

    public MinioServiceImpl(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    @Override
    public void upload(byte[] content) {
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .stream(new ByteArrayInputStream(content), content.length,-1)
                    .build();
            minioClient.putObject(putObjectArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
