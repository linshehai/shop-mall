package com.hy.mall.file;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "file")
public class FileConfig {

    private String filesystem;

    private String server;

    private String username;

    private String password;

    @Bean
    public MinioClient createClient(){

        return MinioClient.builder()
                .endpoint(server)
                .credentials(username,password)
                .build();
    }

    @ConditionalOnClass(name = {"io.minio.MinioClient"})
    @Bean
    public FileService fileService(MinioClient minioClient){
        return new MinioServiceImpl(minioClient);
    }
}
