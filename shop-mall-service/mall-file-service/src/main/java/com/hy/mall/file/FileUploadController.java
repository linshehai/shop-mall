package com.hy.mall.file;

import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import io.minio.http.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/file")
public class FileUploadController {

    @Autowired
    private FileService fileService;
    @GetMapping("/upload")
    public String upload(){

        return "0";
    }

    @GetMapping("/url/{objectName}")
    public String url(@PathVariable("objectName") String objectName){
        GetPresignedObjectUrlArgs args = GetPresignedObjectUrlArgs.builder().bucket("mall-bucket").object(objectName)
                .method(Method.GET).build();
        try {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }
}
