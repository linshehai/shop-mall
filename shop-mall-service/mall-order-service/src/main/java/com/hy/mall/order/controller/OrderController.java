package com.hy.mall.order.controller;


import com.hy.mall.order.model.Order;
import com.hy.mall.order.service.OrderService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @PostMapping("/createOrder")
    public Result create(Order order){
        String userName = "kevin";
        orderService.createOrder(userName,order);
        return Result.ok(order);
    }
}

