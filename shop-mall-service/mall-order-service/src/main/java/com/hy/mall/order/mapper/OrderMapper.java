package com.hy.mall.order.mapper;

import com.hy.mall.order.model.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
public interface OrderMapper extends BaseMapper<Order> {

}
