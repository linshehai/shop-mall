package com.hy.mall.order.service;

import com.hy.mall.exception.InsufficientStockException;
import com.hy.mall.order.model.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
public interface OrderService extends IService<Order> {

    /**
     * 实现用户下单
     * @param userName 用户名
     * @param order 订单对象
     */
    void createOrder(String userName, Order order);
}
