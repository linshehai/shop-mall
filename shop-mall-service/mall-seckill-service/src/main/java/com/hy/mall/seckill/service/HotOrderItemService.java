package com.hy.mall.seckill.service;

import com.hy.mall.seckill.model.HotOrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
public interface HotOrderItemService extends IService<HotOrderItem> {

}
