package com.hy.mall.seckill.service.impl;

import com.hy.mall.seckill.model.HotOrderItem;
import com.hy.mall.seckill.mapper.HotOrderItemMapper;
import com.hy.mall.seckill.service.HotOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
@Service
public class HotOrderItemServiceImpl extends ServiceImpl<HotOrderItemMapper, HotOrderItem> implements HotOrderItemService {

}
