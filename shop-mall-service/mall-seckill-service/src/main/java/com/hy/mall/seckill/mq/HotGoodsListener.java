package com.hy.mall.seckill.mq;

import com.hy.mall.seckill.service.HotOrderService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@RocketMQMessageListener(
        consumerGroup = "hot-goods-consumer-group",
        topic = "order-queue",
        selectorExpression = "*"
)
@Component
public class HotGoodsListener implements RocketMQListener<String> {


    @Autowired
    private HotOrderService hotOrderService;
    @Override
    public void onMessage(String s) {

        System.out.println(s);
    }
}
