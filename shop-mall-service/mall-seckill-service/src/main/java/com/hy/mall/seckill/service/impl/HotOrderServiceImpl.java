package com.hy.mall.seckill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.hy.mall.seckill.model.HotOrder;
import com.hy.mall.seckill.mapper.HotOrderMapper;
import com.hy.mall.seckill.service.HotOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
@Service
public class HotOrderServiceImpl extends ServiceImpl<HotOrderMapper, HotOrder> implements HotOrderService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private HotOrderMapper hotOrderMapper;

    /**
     * 热门商品抢单
     * @param skuId
     * @param num
     */
    public void createOrder(Long skuId,int num){
        RLock lock = redissonClient.getLock("hotLock");
        //抢购下单加锁
        lock.lock(1, TimeUnit.SECONDS);
        try {
            Object stock = redisTemplate.boundHashOps("hotGoods").get(skuId);
            if (stock == null || Integer.valueOf(stock + "") < num) {
                //库存不足
                return;
            }
            //库存扣减
            Long remaining = redisTemplate.boundHashOps("hotGoods").increment(skuId,-num);

            //如果已经售罄，加入到布隆过滤器，提高抢购商品售罄判断速度
            if (remaining==null||remaining<=0) {
                RBloomFilter<Long> bloomFilter = redissonClient.getBloomFilter("");
                bloomFilter.add(skuId);
            }
            long orderId = IdWorker.getId();
            HotOrder hotOrder = new HotOrder();
            hotOrder.setCreateTime(LocalDateTime.now());
            hotOrder.setUpdateTime(LocalDateTime.now());
            hotOrder.setId(orderId);

            hotOrderMapper.insert(hotOrder);
        }finally {
            //释放锁
            lock.unlock();
        }

    }
}
