package com.hy.mall.seckill.controller;

import com.hy.mall.utils.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/seckill")
public class SeckillController {

    /**
     * 非热门商品抢购
     * @param skuId
     * @param num
     * @return
     */
    @PostMapping("/orderNow")
    public Result<String> orderNow(Long skuId, int num, @RequestHeader("account") String account,HttpServletRequest request){
        System.out.println(account);
        return Result.ok("抢购成功:"+request.getServerPort()+"--"+request.getLocalPort());
    }
}
