package com.hy.mall.druid.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.mall.druid.model.PopularSku;

public interface PopularSkuService extends IService<PopularSku> {
}
