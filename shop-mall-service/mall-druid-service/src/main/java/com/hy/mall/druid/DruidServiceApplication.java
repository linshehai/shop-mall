package com.hy.mall.druid;

import com.alibaba.fastjson.JSON;
import com.hy.mall.druid.model.PopularSku;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.time.LocalDateTime;
@MapperScan(basePackages = "com.hy.mall.druid.mapper")
@SpringBootApplication
public class DruidServiceApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(DruidServiceApplication.class,args);
    }

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Override
    public void run(String... args) throws Exception {
        PopularSku popularSku = new PopularSku();
        popularSku.setIp("192.168.1.21");
        popularSku.setAccessTime(LocalDateTime.now());
        popularSku.setUri("/detail/index.html");
//        Message<String> message = MessageBuilder.withPayload(JSON.toJSONString(popularSku)).build();
        kafkaTemplate.send("goodsdetail",JSON.toJSONString(popularSku));
    }
}
