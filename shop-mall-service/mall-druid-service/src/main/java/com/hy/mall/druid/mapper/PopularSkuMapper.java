package com.hy.mall.druid.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hy.mall.druid.model.PopularSku;

public interface PopularSkuMapper extends BaseMapper<PopularSku> {
}
