package com.hy.mall.goods.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.mall.goods.model.Brand;
import com.hy.mall.goods.service.BrandService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brand")
public class BrandController {


    @Autowired
    private final BrandService brandService;

    @InitBinder("brand")
    public void initBinder(WebDataBinder binder){

    }
    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @PostMapping("/add")
    public Result add(Brand brand){
        brandService.save(brand);
        return Result.ok();
    }
    @PutMapping("/update")
    public Result update(Brand brand){
        brandService.save(brand);
        return Result.ok();
    }
    @GetMapping("/{id}")
    public Result get(@PathVariable("id") Integer id){
        Brand brand = brandService.getById(id);
        return Result.ok(brand);
    }
    @PostMapping("/{id}")
    public Result delete(@PathVariable("id") Integer id){
        brandService.removeById(id);
        return Result.ok();
    }

    @GetMapping("/list")
    public Result brandPage(Brand brand){
        Page<Brand> searchPage = new Page<>();
        searchPage.setMaxLimit(brand.getPageSize());
        searchPage.setCurrent(brand.getPageNum());
        QueryWrapper<Brand> query = new QueryWrapper<>();
        if(StringUtils.hasText(brand.getKeyword())){
            query.likeRight("name",brand.getKeyword());
        }
        IPage<Brand> brandPage = brandService.page(searchPage,query);
        return Result.okPage(brandPage,brandPage.getTotal());
    }
}
