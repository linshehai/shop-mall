package com.hy.mall.goods.mapper;

import com.hy.mall.goods.model.Sku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
public interface SkuMapper extends BaseMapper<Sku> {

    @Update("update sku set num=num-#{num} where id=#{skuId} and num>#{num}")
    int updateStock(@Param("skuId") Long skuId,@Param("num") int num);
}
