package com.hy.mall.goods.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.mall.exception.InsufficientStockException;
import com.hy.mall.goods.model.Brand;
import com.hy.mall.goods.model.Sku;
import com.hy.mall.goods.service.SkuService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
@RestController
@RequestMapping("/sku")
public class SkuController {


    @Autowired
    private SkuService skuService;

    @PutMapping("/deductStock")
    public Result deductStock(@RequestParam("skuId") Long skuId, @RequestParam("num")int num){
        try {
            int nums = skuService.deductStock(skuId, num);
            return Result.ok(nums);
        }catch (InsufficientStockException e){
            return Result.error("库存不足");
        }catch (Exception ex){
            return Result.error();
        }

    }

    @GetMapping("/list")
    public Result skuPage(Sku sku){
        Page<Sku> searchPage = new Page<>();
        searchPage.setMaxLimit(sku.getPageSize());
        searchPage.setCurrent(sku.getPageNum());
        QueryWrapper<Sku> query = new QueryWrapper<>();
        if(StringUtils.hasText(sku.getKeyword())){
            query.likeRight("name",sku.getKeyword());
        }
        IPage<Sku> skuPage = skuService.page(searchPage,query);
        return Result.okPage(skuPage,skuPage.getTotal());
    }

    @GetMapping("/{spuId}")
    public Result skuList(@PathVariable("spuId") Long spuId){
        QueryWrapper<Sku> query = new QueryWrapper<>();
        query.eq("spu_id",spuId);
        List<Sku> list = skuService.list(query);
        return Result.ok(list);
    }


    @GetMapping("/adItem/itemList")
    public Result<List<Sku>> skuList(Integer categoryId){
        return Result.ok(skuService.skuListByCategoryId(categoryId));
    }

    @DeleteMapping("/adItem/{categoryId}")
    public Result remove(@PathVariable("categoryId") Integer categoryId){
        skuService.removeByCategoryId(categoryId);
        return Result.ok();
    }

    @PutMapping("/adItem/{categoryId}")
    public Result update(@PathVariable("categoryId") Integer categoryId){
        skuService.updateByCategoryId(categoryId);
        return Result.ok();
    }
}

