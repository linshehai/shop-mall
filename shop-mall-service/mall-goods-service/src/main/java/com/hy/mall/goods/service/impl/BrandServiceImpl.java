package com.hy.mall.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.mall.goods.mapper.BrandMapper;
import com.hy.mall.goods.model.Brand;
import com.hy.mall.goods.service.BrandService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper,Brand> implements BrandService {


}
