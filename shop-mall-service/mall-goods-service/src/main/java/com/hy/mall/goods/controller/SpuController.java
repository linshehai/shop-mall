package com.hy.mall.goods.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.mall.goods.model.Spu;
import com.hy.mall.goods.model.Spu;
import com.hy.mall.goods.service.SpuService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
@RestController
@RequestMapping("/product")
public class SpuController {

    @Autowired
    private SpuService spuService;
    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Spu spu){
        spuService.addSpu(spu);
        return Result.ok();
    }

    @GetMapping("/list")
    public Result productPage(Spu spu){
        Page<Spu> searchPage = new Page<>();
        searchPage.setMaxLimit(spu.getPageSize());
        searchPage.setCurrent(spu.getPageNum());
        QueryWrapper<Spu> query = new QueryWrapper<>();
        if(StringUtils.hasText(spu.getKeyword())){
            query.likeRight("name",spu.getKeyword());
        }
        IPage<Spu> skuPage = spuService.page(searchPage,query);
        return Result.okPage(skuPage,skuPage.getTotal());
    }
}

