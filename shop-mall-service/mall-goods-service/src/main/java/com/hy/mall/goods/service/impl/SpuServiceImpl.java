package com.hy.mall.goods.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.mall.goods.mapper.BrandMapper;
import com.hy.mall.goods.mapper.SkuMapper;
import com.hy.mall.goods.mapper.SpuMapper;
import com.hy.mall.goods.model.Brand;
import com.hy.mall.goods.model.Sku;
import com.hy.mall.goods.model.Spu;
import com.hy.mall.goods.service.SpuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu> implements SpuService {

    @Autowired
    private SpuMapper spuMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public void addSpu(Spu spu){
        Long spuId = IdWorker.getId();
        spu.setId(spuId);
        spu.setIsDelete(0);
        spuMapper.insert(spu);
        Brand brand = brandMapper.selectById(spu.getBrandId());
        Integer categoryId = spu.getCategoryThreeId();
        List<Sku> skuList = spu.getSkuList();
        if(skuList!=null) {
            for (Sku sku : skuList) {
                sku.setId(IdWorker.getId());
                sku.setSpuId(spuId);
                sku.setCategoryId(categoryId);
                sku.setCreateTime(LocalDateTime.now());
                sku.setUpdateTime(LocalDateTime.now());
                sku.setBrandId(spu.getBrandId());
                sku.setBrandName(brand.getName());
                skuMapper.insert(sku);
            }
        }
    }
}
