package com.hy.mall.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.mall.exception.InsufficientStockException;
import com.hy.mall.goods.mapper.SkuMapper;
import com.hy.mall.goods.model.Sku;
import com.hy.mall.goods.service.SkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "ad_sku_list")
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements SkuService {


    @Autowired
    private SkuMapper skuMapper;
    @Override
    public int deductStock(Long skuId, int num) throws InsufficientStockException {
        SkuMapper skuMapper = getBaseMapper();
        int nums = skuMapper.updateStock(skuId,num);
        if(nums<=0){
            throw new InsufficientStockException(skuId+":库存不足");
        }
        return nums;
    }

    @Cacheable(key = "#categoryId")
    @Override
    public List<Sku> skuListByCategoryId(Integer categoryId) {
        System.out.println("查询数据库");
        QueryWrapper<Sku> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id",categoryId);
        return skuMapper.selectList(queryWrapper);
    }

    @CacheEvict(key = "#categoryId")
    @Override
    public void removeByCategoryId(Integer categoryId) {

    }

    @CachePut(key = "#categoryId")
    @Override
    public List<Sku> updateByCategoryId(Integer categoryId) {
        QueryWrapper<Sku> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id",categoryId);
        return skuMapper.selectList(queryWrapper);
    }

}
