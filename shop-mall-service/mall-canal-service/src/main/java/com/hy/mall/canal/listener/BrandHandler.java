package com.hy.mall.canal.listener;

import com.hy.mall.goods.model.Brand;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.annotation.CanalTable;
import top.javatool.canal.client.handler.EntryHandler;

@Component
@CanalTable("brand")
public class BrandHandler implements EntryHandler<Brand> {


    @Override
    public void insert(Brand brand) {
        System.out.println(brand);
    }

    @Override
    public void update(Brand before, Brand after) {
        System.out.println("before"+before);
        System.out.println("after"+after);
    }

    @Override
    public void delete(Brand brand) {
        System.out.println(brand);
    }
}
