package com.hy.mall.user.mapper;

import com.hy.mall.user.model.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
public interface AddressMapper extends BaseMapper<Address> {

}
