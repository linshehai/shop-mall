package com.hy.mall.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hy.mall.user.model.UserRole;
import com.hy.mall.user.mapper.UserRoleMapper;
import com.hy.mall.user.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private UserRoleService userRoleService;
    @Override
    public void updateUserRoles(Long userId, List<Integer> roleIds) {
        if(roleIds!=null){
            QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id",userId);
            userRoleMapper.delete(queryWrapper);
            List<UserRole> userRoles =roleIds.stream().filter(roleId->roleId!=null).map(roleId->new UserRole(userId,roleId)).collect(Collectors.toList());
            userRoleService.saveBatch(userRoles);
        }

    }
}
