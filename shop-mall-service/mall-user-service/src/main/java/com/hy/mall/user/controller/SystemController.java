package com.hy.mall.user.controller;

import com.alibaba.nacos.common.utils.MD5Utils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.model.User;
import com.hy.mall.user.service.MenuService;
import com.hy.mall.user.service.RoleService;
import com.hy.mall.user.service.UserRoleService;
import com.hy.mall.user.service.UserService;
import com.hy.mall.utils.IpUtil;
import com.hy.mall.utils.JwtTokenUtils;
import com.hy.mall.utils.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.bouncycastle.jcajce.provider.digest.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
@ApiSupport(author = "linshehai@qq.com")
public class SystemController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @ApiOperationSupport(ignoreParameters = {"userRoles","menus"})
    @ApiOperation(value = "登陆接口")
    @ApiImplicitParam(name = "user",value = "用户对象",required = true)
    @PostMapping("/login")
    public Result<User> login(@RequestBody User user, HttpServletRequest request){
        String username = user.getUsername();
        String password = user.getPassword();

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);
        User dbUser = userService.getOne(queryWrapper);
        //用户存在
        if(dbUser!=null&&password.equals(dbUser.getPassword())){
            Map<String,Object> claim = new HashMap();
            claim.put("userId",dbUser.getId());
            claim.put("username",dbUser.getUsername());
            if(dbUser.getUserRoles()!=null){
                claim.put("roles",dbUser.getUserRoles());
            }
            String clientIp = IpUtil.getIpAddr(request);
            claim.put("ip", MD5Utils.encodeHexString(clientIp.getBytes(StandardCharsets.UTF_8)));
            String token = JwtTokenUtils.createToken(claim);
            dbUser.setLoginTime(LocalDateTime.now());
            dbUser.setUpdateTime(dbUser.getLoginTime());
            userService.updateById(dbUser);
            return Result.ok(token);
        }else{
            return Result.error("1","账号或密码错误");
        }
    }

    @GetMapping("/info")
    public Result<User> info(@RequestHeader("userId") Long userId){
        User user = userService.getById(userId);
        List<Role> userRoles = userService.getUserRoles(userId);
        user.setUserRoles(userRoles);
        List<Menu> list = userService.getUserMenus(userId);
        user.setMenus(list);
        return Result.ok(user);
    }


    @ApiOperationSupport(ignoreParameters = {"userRoles","menus"})
    @GetMapping("/list")
    public Result<User> list(User user){
        Page<User> searchPage = new Page<>();
        searchPage.setMaxLimit(user.getPageSize());
        searchPage.setCurrent(user.getPageNum());
        QueryWrapper<User> query = new QueryWrapper<>();
        if(StringUtils.hasText(user.getKeyword())){
            query.likeRight("username",user.getKeyword()).or().likeRight("nick_name",user.getKeyword());
        }

        IPage<User> userPage = userService.page(searchPage,query);
        return Result.okPage(userPage,userPage.getTotal());
    }

    @Autowired
    private UserRoleService userRoleService;


    @PostMapping("/role/update")
    public Result<String> allocRole(@RequestParam("adminId") Long userId,@RequestParam("roleIds") List<Integer> roleIds){
        userRoleService.updateUserRoles(userId,roleIds);
        return Result.ok();
    }

    @ApiOperationSupport(ignoreParameters = {"userRoles","menus"})
    @PostMapping("/update/{id}")
    public Result<String> update(@RequestBody User user,@PathVariable("id") Long userId){
        user.setId(userId);
        userService.updateById(user);
        return Result.ok("更新用户成功");
    }

    @ApiOperationSupport(ignoreParameters = {"userRoles","menus"})
    @PostMapping("/register")
    public Result<String> createUser(@RequestBody User user){
        Long userId = IdWorker.getId();
        user.setId(userId);
        user.setCreateTime(LocalDateTime.now());
        userService.save(user);
        return Result.ok("用户添加成功");
    }
}
