package com.hy.mall.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hy.mall.user.mapper.RoleMenuMapper;
import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.mapper.RoleMapper;
import com.hy.mall.user.model.RoleMenu;
import com.hy.mall.user.service.RoleMenuService;
import com.hy.mall.user.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Autowired
    private RoleMenuService roleMenuService;

    @Override
    public int changeRoleStatus(Integer id, Integer status) {
        return roleMapper.updateStatus(id,status);
    }

    @Override
    public List<Menu> getRoleMenus(Integer roleId) {
        return roleMapper.selectRoleMenus(roleId);
    }

    @Override
    public int bindMenuToRole(Integer roleId, List<Integer> menuIds) {
        if(menuIds!=null){
            QueryWrapper delWrapper = new QueryWrapper();
            delWrapper.eq("role_id",roleId);
            int count = roleMenuMapper.delete(delWrapper);
            if(count>0){
               List<RoleMenu> roleMenus = menuIds.stream().filter(menuId->menuId!=null).map(menuId-> new RoleMenu(roleId,menuId)).collect(Collectors.toList());
                roleMenuService.saveBatch(roleMenus);
                return menuIds.size();
            }
        }
        return 0;
    }
}
