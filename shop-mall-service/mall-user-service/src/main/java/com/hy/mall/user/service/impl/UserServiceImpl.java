package com.hy.mall.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.model.User;
import com.hy.mall.user.mapper.UserMapper;
import com.hy.mall.user.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Role> getUserRoles(Long userId) {
        return userMapper.selectUserRoles(userId);
    }

    @Override
    public List<Menu> getUserMenus(Long userId) {
        return userMapper.selectUserMenus(userId);
    }
}
