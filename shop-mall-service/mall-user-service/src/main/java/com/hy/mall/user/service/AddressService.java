package com.hy.mall.user.service;

import com.hy.mall.user.model.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
public interface AddressService extends IService<Address> {

}
