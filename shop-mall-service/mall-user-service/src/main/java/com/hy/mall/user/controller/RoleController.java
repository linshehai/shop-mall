package com.hy.mall.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.model.User;
import com.hy.mall.user.service.RoleService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/listAll")
    public Result<Role> allRoles(){
        return Result.ok(roleService.list());
    }

    @GetMapping("/{id}")
    public Result<Role> getRole(@PathVariable("id") Integer roleId){
        return Result.ok(roleService.getById(roleId));
    }

    @GetMapping("/list")
    public Result<Role> rolePage(Role role){
        Page<Role> searchPage = new Page<>();
        searchPage.setMaxLimit(role.getPageSize());
        searchPage.setCurrent(role.getPageNum());
        QueryWrapper<Role> query = new QueryWrapper<>();
        if(StringUtils.hasText(role.getKeyword())){
            query.likeRight("name",role.getKeyword());
        }
        IPage<Role> rolePage = roleService.page(searchPage,query);
        return Result.okPage(rolePage,rolePage.getTotal());
    }

    @PostMapping("/create")
    public Result<String> create(@RequestBody Role role){
        roleService.save(role);
        return Result.ok();
    }

    @PostMapping("/update/{id}")
    public Result<String> update(@RequestBody Role role,@PathVariable("id") Integer id){
        role.setId(id);
        roleService.updateById(role);
        return Result.ok();
    }

    @GetMapping("/listMenu/{roleId}")
    public Result<Menu> listMenu(@PathVariable("roleId") Integer roleId){
        List<Menu> roleMenus = roleService.getRoleMenus(roleId);
        return Result.ok(roleMenus);
    }

    @PostMapping("/updateStatus/{id}")
    public Result<String> updateStatus(@RequestParam("status") Integer status, @PathVariable("id") Integer id){
        int count = roleService.changeRoleStatus(id,status);
        return Result.ok(count);
    }

    @PostMapping("/allocMenu")
    public Result<Integer> allocMenu(@RequestParam("roleId") Integer roleId,@RequestParam("menuIds") List<Integer> menuIds){
        int count = roleService.bindMenuToRole(roleId,menuIds);
        return Result.ok(count);
    }
}

