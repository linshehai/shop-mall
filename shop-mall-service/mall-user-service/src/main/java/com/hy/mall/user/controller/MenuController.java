package com.hy.mall.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.service.MenuService;
import com.hy.mall.utils.Result;
import com.hy.mall.utils.model.Hierarchy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @InitBinder("menu")
    private void initBinder(WebDataBinder binder) {
    }

    @GetMapping("/treeList")
    public Result<Menu> menuTree(){
        List<Menu> menuList = menuService.list();
        Map<Integer,Menu> map = new HashMap<>();
        menuList.forEach(menu -> {
           map.put(menu.getId(),menu);
        });
        Iterator<Menu> menuIterator = menuList.iterator();
        while (menuIterator.hasNext()){
            Menu menu = menuIterator.next();
            Menu parent = map.get(menu.getParentId());
            if(parent!=null){
                parent.addChild(menu);
                menuIterator.remove();
            }
        }
        return Result.ok(menuList);
    }

    @PostMapping("/create")
    public Result<String> create(@RequestBody Menu menu, @RequestHeader("userId") Long userId){
        menu.setCreateTime(LocalDateTime.now());
        menu.setUpdateTime(LocalDateTime.now());
        menu.setCreator(userId);
        menu.setStatus(1);
        menuService.save(menu);
        return Result.ok();
    }

    @GetMapping("/{id}")
    public Result<Menu> getMenu(@PathVariable("id") Integer menuId){
        return Result.ok(menuService.getById(menuId));
    }

    @PostMapping("/update/{id}")
    public Result<Boolean> updateMenu(@RequestBody Menu menu,@PathVariable("id") Integer id){
       return Result.ok(menuService.updateById(menu));
    }

    @PostMapping("/delete/{id}")
    public Result<Boolean> deleteMenu(@PathVariable("id") Integer id){
       return Result.ok(menuService.removeById(id));
    }

    @GetMapping("/list/{parentId}")
    public Result menuList(Menu menu, @PathVariable("parentId") Integer parentId){
        Page<Menu> searchPage = new Page<>();
        searchPage.setMaxLimit(menu.getPageSize());
        searchPage.setCurrent(menu.getPageNum());
        QueryWrapper<Menu> query = new QueryWrapper<>();
        query.eq("parent_id",parentId);
        if(StringUtils.hasText(menu.getKeyword())){
            query.likeRight("name",menu.getKeyword());
        }
        IPage<Menu> menuPage = menuService.page(searchPage,query);
        return Result.okPage(menuPage,menuPage.getTotal());
    }
}

