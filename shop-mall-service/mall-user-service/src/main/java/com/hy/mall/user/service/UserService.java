package com.hy.mall.user.service;

import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
public interface UserService extends IService<User> {

    List<Role> getUserRoles(Long userId);

    List<Menu> getUserMenus(Long userId);
}
