package com.hy.mall.user.service;

import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
public interface RoleService extends IService<Role> {

    int changeRoleStatus(Integer id, Integer status);

    List<Menu> getRoleMenus(Integer roleId);

    int bindMenuToRole(Integer roleId, List<Integer> menuIds);
}
