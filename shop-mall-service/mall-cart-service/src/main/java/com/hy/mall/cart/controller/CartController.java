package com.hy.mall.cart.controller;

import com.hy.mall.cart.model.Cart;
import com.hy.mall.cart.service.CartService;
import com.hy.mall.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {


    @Autowired
    private CartService cartService;

    @PostMapping("/add")
    public Result add(@RequestBody Cart cart){
        cartService.add(cart);
        return Result.ok();
    }

    @GetMapping("/{id}")
    public Result get(@PathVariable("id") Long id){
        Cart cart = cartService.getById(id);
        return Result.ok(cart);
    }

    @GetMapping("/cartList")
    public Result<List<Cart>> cartList(){
        List<Cart> carts = cartService.getCartList("kevin");
        return Result.ok(carts);
    }

    @GetMapping("/getCartList")
    public List<Cart> getCartList(List<Long> cartIds){
        List<Cart> carts = cartService.getCartList(cartIds);
        return carts;
    }

    @DeleteMapping("/removeCartByIds")
    void clearOrderedCartInfo(List<Long> cartIds){
        cartService.clearCartInfoByIds(cartIds);
    }
}
