package com.hy.mall.cart.service.impl;

import com.hy.mall.cart.mapper.CartMapper;
import com.hy.mall.cart.model.Cart;
import com.hy.mall.cart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartMapper cartMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void add(Cart cart){
        cartMapper.insert(cart);
    }


    @Override
    public Cart getById(Long id){
        Optional<Cart> cart = cartMapper.findById(id);
        return cart.orElse(null);
    }

    @Override
    public List<Cart> getCartList(String userName) {
        Cart cart = new Cart();
        cart.setUserName(userName);
        Example<Cart> cartExample = Example.of(cart);
        Sort sort = Sort.by("skuId").ascending();
        return cartMapper.findAll(cartExample,sort);
    }

    @Override
    public List<Cart> getCartList(List<Long> cartIds) {
        return  (List<Cart>)cartMapper.findAllById(cartIds);
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void clearCartInfoByIds(List<Long> cartIds) {
        for (Long cartId : cartIds) {
            cartMapper.deleteById(cartId);
        }

    }
}
