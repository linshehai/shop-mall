package com.hy.mall.cart.mapper;

import com.hy.mall.cart.model.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CartMapper extends MongoRepository<Cart,Long> {
}
