package com.hy.mall.cart.service;

import com.hy.mall.cart.model.Cart;

import java.util.List;

public interface CartService {
    void add(Cart cart);

    Cart getById(Long id);

    List<Cart> getCartList(String userName);

    List<Cart> getCartList(List<Long> cartIds);

    void clearCartInfoByIds(List<Long> cartIds);
}
