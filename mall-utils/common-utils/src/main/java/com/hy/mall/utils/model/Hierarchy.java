package com.hy.mall.utils.model;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Hierarchy<T> extends SearchInfo{

    @TableField(exist = false)
    private T root;

    @TableField(exist = false)
    private List<T> children;

    public Hierarchy(){
        this.children = new ArrayList<>();
    }
    public void addChild(T child) {
        this.children.add(child);
    }
}
