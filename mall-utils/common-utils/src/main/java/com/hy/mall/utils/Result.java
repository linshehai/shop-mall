package com.hy.mall.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {

    private static String MSG = "请求成功";

    private String code;

    private String msg;

    private T data;

    private long total;

    public static Result ok(){
        return new Result("200",MSG,null,0);
    }

    public static Result ok(String code,String msg){
        return new Result(code,msg,null,0);
    }
    public static <T> Result ok(T data){
        return new Result("200",MSG,data,0);
    }
    public static <T> Result okPage(T data,long total){
        return new Result("200",MSG,data,total);
    }


    public static Result error(){
        return new Result("300","请求失败",null,0);
    }

    public static Result error(String code,String msg){
        return new Result(code,msg,null,0);
    }

    public static <T> Result error(T data){
        return new Result("300","请求失败",data,0);
    }
}
