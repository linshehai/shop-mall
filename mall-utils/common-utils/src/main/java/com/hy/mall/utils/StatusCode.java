package com.hy.mall.utils;

/**
 * 状态码
 */
public class StatusCode {

    public static final String SUCCESS = "200";
    public static final String ERROR = "300";

}
