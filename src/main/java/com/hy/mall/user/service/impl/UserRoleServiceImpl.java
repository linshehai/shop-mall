package com.hy.mall.user.service.impl;

import com.hy.mall.user.model.UserRole;
import com.hy.mall.user.mapper.UserRoleMapper;
import com.hy.mall.user.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
