package com.hy.mall.user.service.impl;

import com.hy.mall.user.model.Menu;
import com.hy.mall.user.mapper.MenuMapper;
import com.hy.mall.user.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

}
