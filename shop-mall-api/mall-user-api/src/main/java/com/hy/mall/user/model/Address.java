package com.hy.mall.user.model;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 详细地址
     */
    private String address;

    private Long userId;

    /**
     * 省
     */
    private String province;

    /**
     * 地市
     */
    private String city;

    /**
     * 区县
     */
    private String county;

    /**
     * 街道
     */
    private String street;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer status;


}
