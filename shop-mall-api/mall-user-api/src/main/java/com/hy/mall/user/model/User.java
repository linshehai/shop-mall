package com.hy.mall.user.model;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hy.mall.utils.model.SearchInfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_user")
@ApiModel(value = "用户模型",description = "用户类，对应用户表")
public class User extends SearchInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    private String password;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
    private LocalDateTime loginTime;

    private String nickName;

    private String email;

    private String note;

    private int status;

    @TableField(exist = false)
    private List<Role> userRoles;
    @TableField(exist = false)
    private List<Menu> menus;
}
