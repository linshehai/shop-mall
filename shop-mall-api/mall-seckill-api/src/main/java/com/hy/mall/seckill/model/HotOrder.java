package com.hy.mall.seckill.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class HotOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String userName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String shippingAddress;

    private String phone;

    private String contacts;

    private LocalDateTime shippingTime;

    private LocalDateTime finishTime;

    private Integer totalNum;

    private BigDecimal totalMoney;


}
