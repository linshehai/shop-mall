package com.hy.mall.seckill.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class HotOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer skuId;

    private String skuName;

    private BigDecimal price;

    private Integer num;

    private BigDecimal totalMoney;

    private Long orderId;


}
