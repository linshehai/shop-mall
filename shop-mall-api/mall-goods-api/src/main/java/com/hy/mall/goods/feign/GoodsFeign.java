package com.hy.mall.goods.feign;

import com.hy.mall.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("mall-goods")
@RequestMapping("/goods")
public interface GoodsFeign {

    @PutMapping("/deductStock")
    Result<Integer> deductStock(@RequestParam("skuId") Long skuId, @RequestParam("num")int num);
}
