package com.hy.mall.goods.model;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hy.mall.utils.model.SearchInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Spu extends SearchInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String intro;

    private Integer brandId;

    private Integer categoryOneId;

    private Integer categoryTwoId;

    private Integer categoryThreeId;

    private String images;

    private String afterSalesService;

    private String content;

    private String attributeList;

    private Integer putAway;

    private Integer isDelete;

    private Integer status;

    private String brandName;

    @TableField(exist = false)
    private List<Sku> skuList;

}
