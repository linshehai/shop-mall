package com.hy.mall.cart.feign;

import com.hy.mall.cart.model.Cart;
import com.hy.mall.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("mall-cart")
@RequestMapping("/cart")
public interface CartFeign {

    @PostMapping("/add")
    Result add(@RequestBody Cart cart);

    @GetMapping("/{id}")
    Result get(@PathVariable("id") Long id);

    @GetMapping("/cartList")
    Result<List<Cart>> cartList();

    @GetMapping("/getCartList")
    List<Cart> getCartList(List<Long> cartIds);

    @DeleteMapping("/removeCartByIds")
    void clearOrderedCartInfo(List<Long> cartIds);
}
